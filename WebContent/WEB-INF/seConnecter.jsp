
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="ENI Ecole">

  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="css/4-col-portfolio.css" rel="stylesheet">

<title>Se connecter</title>


</head>
<body>
	<!-- Navigation -->
	<%@include file="template/navSimple.jsp"%>
	<!-- Fin Navigation -->

	<!-- Content -->
	<!-- Heading -->
	<h3 class="my-4">Se connecter</h3>
	
		<c:if test="${!empty requestScope.errors}">
			<div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
				<div class="card h-100 alert">
					<div class="card-body alert alert-danger">
						<h4 class="card-title">
							<h2>Erreurs</h2>
							<c:forEach var="msg" items="${errors}">
								<p class="card-text">${msg}</p>
							</c:forEach>
						</h4>
					</div>
				</div>
			</div>

		</c:if>
	
	<!-- Fin Heading -->
	<!-- Main -->
	<div class="row">
		
					<form method="post" action="./Connexion">
						<div class="pseudo">
							<label for="pseudo">Pseudo : </label> <input class="form-control"
								id="pseudo" required name="pseudo">
						</div>
						<div class="mdp">
							<label for="motDePasse">Mot de Passe : </label> <input
								type="password" class="form-control" id="motDePasse" required
								name="motDePasse">
						</div>
						<div class="form-group" id="connecte">
										<input type="submit" value="Connection" class="btn btn-primary">
										<fieldset>
									<input type="checkbox" name="sesouvenirdemoi" value="sesouvenirdemoi" >se souvenir de moi <br>
									</fieldset>
						</div>
						<div>
									<a>mot de passe oublié </a>
						</div>
						<div>
										<a  href="${pageContext.request.contextPath}/inscription" class="btn btn-primary"> Créer un compte</a>
									</div>
									
									
					</form>
					
					
				</div>
				
		
	<!-- Fin Main -->
	<!-- Fin Content -->
	<!-- Footer -->
	<%@include file="template/footer.jsp"%>

	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>