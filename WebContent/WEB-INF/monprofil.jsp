<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<%@include file="template/head.jsp"%>

<body>
	<%@include file="template/navConnecte.jsp"%>

	<div class="container footer-demodule justify-content-center">

		<div
			class="col-lg-8 col-md-8 col-sm-8 portfolio-item justify-content-center">

			<ul class="list-group col-12 ">

				<li class="list-group-item d-flex justify-content-between">Pseudo
					: ${utilisateur.pseudo}</li>
				<li class="list-group-item d-flex justify-content-between">Nom
					: ${utilisateur.nom}</li>
				<li class="list-group-item d-flex justify-content-between">Prenom
					: ${utilisateur.prenom}</li>
				<li class="list-group-item d-flex justify-content-between">Email
					: ${utilisateur.email}</li>
				<li class="list-group-item d-flex justify-content-between">Telephone
					: ${utilisateur.telephone}</li>
				<li class="list-group-item d-flex justify-content-between">Rue
					: ${utilisateur.rue}</li>
				<li class="list-group-item d-flex justify-content-between">Code
					postal : ${utilisateur.codePostal}</li>
				<li class="list-group-item d-flex justify-content-between">Ville
					: ${utilisateur.ville}</li>

			</ul>



		</div>
	</div>
	<%@include file="template/footer.jsp"%>
</body>
</html>





