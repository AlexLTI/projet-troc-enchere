<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<%@include file="template/head.jsp"%>

<body>
	<%@include file="template/navDeco.jsp"%>
	<div class="container">
		<h1 class="text-center" >Liste des enchères</h1>
		<div class="col-lg-6 col-md-6 col-sm-6 portfolio-item justify-content-center">
			
			<div>
				<label class = "h3" for="filtres">Filtres : </label> <input type="text"
					placeholder="Le nom de l'article contient" class="form-control"
					id="filtres" required name="filtres">
			</div>
			<div class="float-right">
				<input type="submit" value="Rechercher">
			</div>
			<br>
			<div>
					<c:choose>
					<c:when test="${lstCategories.size()>0}">
				<label for="categories">Categories : </label> 
				<select	name="categories" id="categories">
				<option value="toutes"> Toutes </option>
				<c:forEach var="c" items="${lstCategories}">
				
					<option value="categorie"> ${c.libelle}</option>
				
</c:forEach>
				</select>
				</c:when>
				</c:choose>
				
			</div>
	<br>
			<div class="row">
				<c:choose>
					<c:when test="${lstArticles.size()>0}">
						<ul class="list-group col-12 ">
							<c:forEach var="a" items="${lstArticles}">
								<li
									class="list-group-item d-flex justify-content-between ">
									Article :  ${a.nomArticle}</li>
								<li
									class=" d-flex justify-content-between align-items-center">
									Date de fin d'enchère : ${a.dateFinEncheres}</li>

								<c:forEach var="e" items="${a.lstEncheres}">
									<li
										class=" d-flex justify-content-between align-items-center">
										Enchere en cours : ${e.montantEnchere}</li>


								</c:forEach>
								<li
									class=" d-flex justify-content-between align-items-center">
									Prix de départ : ${a.miseAPrix} points</li>
								<li
									class=" d-flex  align-items-center">
									Vendeur : <a href="${pageContext.servletContext.contextPath}/Profil?pseudo=${a.vendeur.pseudo}"> ${a.vendeur.pseudo}</a></li>
									<br>
							</c:forEach>


						</ul>
					</c:when>


					<c:otherwise>
						<p>Pas de liste actuellement.
						<p>
					</c:otherwise>
				</c:choose>
			</div>


		</div>
	</div>
	<%@include file="template/footer.jsp"%>
</body>
</html>





