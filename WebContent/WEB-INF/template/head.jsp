<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Eni Ecole">
<meta name="author" content="Eni Ecole groupe 5">

<title>ENI-Enchere</title>

<!-- Bootstrap core CSS -->
<link href="${pageContext.servletContext.contextPath}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="${pageContext.servletContext.contextPath}/css/4-col-portfolio.css" rel="stylesheet">
<link rel="icon" href="${pageContext.servletContext.contextPath}/images/favicon.ico">

</head>