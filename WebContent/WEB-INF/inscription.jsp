<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page errorPage="error.jsp" isErrorPage="false"%>

<!DOCTYPE html>
<html>

<head>

<title>Inscription</title>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<link href="css/4-col-portfolio.css" rel="stylesheet">
<link rel="icon" href="images/favicon.ico">

</head>

<body>

	<!-- Navigation -->
	<%@include file="template/navSimple.jsp"%>

	<div class="container">


		<h1 class="my-4">Inscription</h1>


		<%
			if (request.getAttribute("errors") != null) {
			List<String> errors = (List<String>) request.getAttribute("errors");
		%>

		<div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
			<div class="card h-100 alert">
				<div class="card-body alert alert-danger">
					<h4 class="card-title">
						<h2>Erreurs</h2>
						<%
							for (String msg : errors) {
						%>
						<p class="card-text"><%=msg%></p>
						<%
							}
						%>
					</h4>
				</div>
			</div>
		</div>
		<%
			}
		%>
		<div class="row">
			<div class="col">
				<form method="post"
					action="${pageContext.request.contextPath}/inscription">
					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="prenom">Pseudo:</label> <input type="text"
								class="form-control" name="pseudo"
								value="<%=request.getAttribute("pseudo") != null ? request.getAttribute("pseudo") : ""%>"
								required="required">
						</div>
						<div class="form-group col-md-4">
							<label for="nom">Nom :</label> <input type="text"
								class="form-control" name="nom"
								value="<%=request.getAttribute("nom") != null ? request.getAttribute("nom") : ""%>"
								required="required">
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="prenom">Prénom :</label> <input type="text"
								class="form-control" name="prenom"
								value="<%=request.getAttribute("prenom") != null ? request.getAttribute("prenom") : ""%>"
								required="required">
						</div>
						<div class="form-group col-md-4">
							<label for="nom">Email :</label> <input type="email"
								class="form-control" name="email"
								value="<%=request.getAttribute("email") != null ? request.getAttribute("email") : ""%>"
								required="required">
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="prenom">Téléphone :</label> <input type="text"
								class="form-control"
								value="<%=request.getAttribute("telephone") != null ? request.getAttribute("telephone") : ""%>"
								name="telephone">
						</div>
						<div class="form-group col-md-4">
							<label for="nom">Adresse :</label> <input type="text"
								class="form-control" name="adresse"
								value="<%=request.getAttribute("adresse") != null ? request.getAttribute("adresse") : ""%>"
								required="required">
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="prenom">Code Postal :</label> <input type="text"
								class="form-control" name="cp"
								value="<%=request.getAttribute("cp") != null ? request.getAttribute("cp") : ""%>"
								required="required">
						</div>
						<div class="form-group col-md-4">
							<label for="nom">Ville :</label> <input type="text"
								class="form-control" name="ville"
								value="<%=request.getAttribute("ville") != null ? request.getAttribute("ville") : ""%>"
								required="required">
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="prenom">Mot de passe :</label> <input type="password"
								class="form-control" name="password" required="required">
						</div>
						<div class="form-group col-md-4">
							<label for="nom">Confirmation :</label> <input type="password"
								class="form-control" name="confirmMdp" required="required">
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary">Valider</button>
						<a href="${pageContext.request.contextPath}/encheres"
							class="btn btn-primary">Annuler</a>
					</div>
				</form>

			</div>

		</div>



		<!-- /.row -->

	</div>
	<!-- /.container -->

	<!-- Footer -->
	<%@include file="template/footer.jsp"%>
</body>

</html>
