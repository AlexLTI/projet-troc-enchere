/**
 * 
 */
package fr.eni.trocenchere.dal;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Classe en charge d'établir le pool de connection à la base de données
 * 
 * @author mndiaye2020
 * @version TrocEnchere - v1.0
 * @date 21 janv. 2021 - 10:41:08
 */
public class ConnectionProvider {
	private static DataSource dataSource;
	
	
		static {
			Context context;
			try{
				context = new InitialContext();
				ConnectionProvider.dataSource =
						(DataSource) context.lookup("java:comp/env/jdbc/pool_cnx");
			}catch (NamingException e) {
				e.printStackTrace();
				throw new RuntimeException("Accès impossible à la base de données");
			}
		}
		public static Connection getConnection()throws SQLException{
			return ConnectionProvider.dataSource.getConnection();
		}
}
