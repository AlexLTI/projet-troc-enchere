/**
 * 
 */
package fr.eni.trocenchere.dal;

import java.sql.SQLException;
import java.util.List;

import fr.eni.trocenchere.bo.Utilisateur;
import fr.eni.trocenchere.exception.BusinessException;
import fr.eni.trocenchere.exception.DALException;

/**
 * Classe en charge
 * @author mndiaye2020
 * @version TrocEnchere - v1.0
 * @date 21 janv. 2021 - 11:42:07
 */
public interface UtilisateurDAO {
	/**
	 * M�thode en charge de la connection d'un utilisateur existant
	 * 
	 * @param pseudo
	 * @param motDePasse
	 * @return
	 * @throws BusinessException
	 */
	Utilisateur identification(String pseudo, String motDePasse) throws BusinessException;

	/**
	 * M�thode en charge de cr�er utilisateur
	 * 
	 * @param u
	 * @throws DALException
	 */
	void creerUtilisateur(Utilisateur u) throws DALException;

	/**
	 * M�thode en charge de v�rifier l'existance du couple pseudo/mot de passe
	 * 
	 * @param pseudo
	 * @param mail
	 * @param errors
	 * @return
	 * @throws SQLException
	 * @throws DALException
	 */
	List<String> pseudoOuMailExistant(String pseudo, String mail, List<String> errors)
			throws SQLException, DALException;

	/**
	 * M�thode en charge de r�cup�rer le profil d'un utilisateur
	 * 
	 * @param pseudo
	 * @return
	 * @throws SQLException
	 * @throws BusinessException
	 */
	Utilisateur getProfil(String pseudo) throws SQLException, BusinessException;

}
