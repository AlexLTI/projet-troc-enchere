/**
 * 
 */
package fr.eni.trocenchere.dal;

/**
 * Classe en charge de fabriquer les différentes instances. Centralise les
 * instances de la couche DAl
 * 
 * @author mndiaye2020
 * @version TrocEnchere - v1.0
 * @date 21 janv. 2021 - 12:37:33
 */
public class DAOFactory {
	/**
	 * Retourne l'instance de ArrticleDAOJDBCImpl
	 * 
	 * @return ArticleDAO
	 */
	public static ArticleDAO getArticleVenduDAO() {
		return new ArticleJDBCImpl();
	}

	/**
	 * Retourne l'instance de UtilisateurDAOJDBCImpl
	 * 
	 * @return UtilisateurDAO
	 */
	public static UtilisateurDAO getUtilisateurDAO() {
		return new UtilisateurDAOJdbcImpl();
	}
	public static CategorieDAO getCategorieDAO() {
		return new CategorieJDBCImpl();
	}
}
