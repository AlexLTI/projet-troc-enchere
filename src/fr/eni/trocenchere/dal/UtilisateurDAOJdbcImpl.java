/**
 * 
 */
package fr.eni.trocenchere.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import fr.eni.trocenchere.bo.Utilisateur;
import fr.eni.trocenchere.exception.BusinessException;
import fr.eni.trocenchere.exception.DALException;

/**
 * Classe en charge
 * 
 * @author mndiaye2020
 * @version TrocEnchere - v1.0
 * @date 21 janv. 2021 - 12:38:42
 */
public class UtilisateurDAOJdbcImpl implements UtilisateurDAO {
	private static final String CONNECTION = "Select pseudo, mot_de_passe, nom, prenom, email, telephone, rue, code_postal, ville from UTILISATEURS where pseudo = ? and mot_de_passe = ?";
	private static final String CREATE = "INSERT INTO UTILISATEURS "
			+ " (pseudo, nom, prenom, email, telephone, rue, code_postal, ville, mot_de_passe, credit, administrateur)"
			+ " VALUES(?,?,?,?,?,?,?,?,?,?,?)";
	private static final String PSEUDO = "SELECT pseudo FROM UTILISATEURS WHERE pseudo = ?";
	private static final String MAIL = "SELECT email FROM UTILISATEURS WHERE email = ?";
	private static final String AFFICHER_PROFIL = "select pseudo, nom, prenom, email, telephone, rue, code_postal, ville from UTILISATEURS where pseudo = ?";

	/**
	 * Méthode en charge de vérifier si le couple pseudo/mot de passe 
	 * correspond bien à enregistrer un utilisateur en DB
	 */
	@Override
	public Utilisateur identification(String pseudo, String motDePasse) throws BusinessException {
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(CONNECTION);
			pstmt.setString(1, pseudo);
			pstmt.setString(2, motDePasse);

			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				Utilisateur u = new Utilisateur();
				u.setPseudo(rs.getString("pseudo"));
				u.setNom(rs.getString("nom"));
				u.setPrenom(rs.getString("prenom"));
				u.setEmail(rs.getString("email"));
				u.setTelephone(rs.getString("telephone"));
				u.setRue(rs.getString("rue"));
				u.setCodePostal(rs.getString("code_postal"));
				u.setVille(rs.getString("ville"));
			
				return u;
			} else {
				BusinessException be = new BusinessException();
				be.addError("pseudo ou mot de passe inconnu");
				throw be;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			BusinessException be = new BusinessException();
			be.addError("Error DB");
			throw be;
		}
	}
	
	/**
	 * Méthode en charge de créer un nouvel utilisateur dans la DB
	 */
	@Override

	public void creerUtilisateur(Utilisateur u) throws DALException {

		try (Connection cnx = ConnectionProvider.getConnection();
				PreparedStatement stmt = cnx.prepareStatement(CREATE, Statement.RETURN_GENERATED_KEYS);) {

			stmt.setString(1, u.getPseudo());
			stmt.setString(2, u.getNom());
			stmt.setString(3, u.getPrenom());
			stmt.setString(4, u.getEmail());
			stmt.setString(5, u.getTelephone());
			stmt.setString(6, u.getRue());
			stmt.setString(7, u.getCodePostal());
			stmt.setString(8, u.getVille());
			stmt.setString(9, u.getMotDePasse());
			stmt.setInt(10, 100);
			stmt.setInt(11, 1);

			stmt.executeUpdate();

		} catch (SQLException e) {
			throw new DALException("Erreur à la création d'utilisateur - " + u, e);
		}

	}

	/**
	 * Méthode en charge de récupérer le profil d'un utilisateur avec son pseudo
	 */
	public Utilisateur getProfil(String pseudo) throws BusinessException {
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(AFFICHER_PROFIL);
			pstmt.setString(1, pseudo);
			ResultSet rs = pstmt.executeQuery();

			if (rs.next()) {
				Utilisateur u = new Utilisateur();
				u.setPseudo(rs.getString("pseudo"));
				u.setNom(rs.getString("nom"));
				u.setPrenom(rs.getString("prenom"));
				u.setEmail(rs.getString("email"));
				u.setTelephone(rs.getString("telephone"));
				u.setRue(rs.getString("rue"));
				u.setCodePostal(rs.getString("code_postal"));
				u.setVille(rs.getString("ville"));
				return u;
			} else {
				BusinessException be = new BusinessException();
				be.addError("utilisateur inconnu");
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
			BusinessException be = new BusinessException();
			be.addError("Error DB");
			throw be;
		}
		return null;

	}

	/**
	 * Méthode en charge de vérifier si le pseudo et le mail 
	 * ne sont pas déjà existant lors de la création d'un nouvel utilisateur
	 */
	@Override
	public List<String> pseudoOuMailExistant(String pseudo, String mail, List<String> errors)
			throws DALException, SQLException {
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmtPseudo = cnx.prepareStatement(PSEUDO);
			pstmtPseudo.setString(1, pseudo);
			ResultSet rsPseudo = pstmtPseudo.executeQuery();

			if (rsPseudo != null && rsPseudo.next()) {

				errors.add("pseudo déjà existant");

			}
			PreparedStatement pstmtMail = cnx.prepareStatement(MAIL);
			pstmtMail.setString(1, mail);
			ResultSet rsMail = pstmtMail.executeQuery();

			if (rsMail != null && rsMail.next()) {

				errors.add("mail déjà existant");

			}
			return errors;
		} catch (SQLException e) {
			throw new SQLException("Erreur à la vérification pseudo ou mail - ", e);
		}
	}

}
