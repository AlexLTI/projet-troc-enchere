package fr.eni.trocenchere.dal;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.eni.trocenchere.bo.Article;
import fr.eni.trocenchere.bo.Enchere;
import fr.eni.trocenchere.bo.Utilisateur;

public class ArticleJDBCImpl implements ArticleDAO {

	private static final String SELECT_ALL_ACCUEIL = "select a.no_article,a.nom_article, a.prix_initial, "
			+ "a.date_fin_encheres, u.pseudo , e.montant_enchere from ARTICLES_VENDUS a "
			+ "inner join UTILISATEURS u on a.no_utilisateur=u.no_utilisateur inner join ENCHERES e "
			+ "on u.no_utilisateur=e.no_utilisateur";



	/**
	 * M�thode en charge de r�cup�rer toutes les ench�res en cours Et retourne une
	 * liste d'ench�res
	 */
	@Override
	public List<Article> findAll() {
		List<Article> listeArticles = new ArrayList<Article>();

		try (Connection cnx = ConnectionProvider.getConnection()) {
			Statement stmt = cnx.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_ALL_ACCUEIL);

			Article articleCourant = new Article();
			while (rs.next()) {
				if (rs.getInt("no_article") != articleCourant.getNoArticle()) {
					articleCourant = articleBuilder(rs);
					listeArticles.add(articleCourant);

				}
				Enchere enchereCourante = enchereBuilder(rs);
				articleCourant.getLstEncheres().add(enchereCourante);

				Utilisateur u = utilisateurBuilder(rs);
				articleCourant.setVendeur(u);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listeArticles;

	}

	/**
	 * Article Builder
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private Article articleBuilder(ResultSet rs) throws SQLException {
		Article article = new Article();
		article.setNoArticle(rs.getInt("no_article"));
		article.setNomArticle(rs.getString("nom_article"));
		article.setDateFinEncheres(rs.getDate("date_fin_encheres").toLocalDate());
		article.setMiseAPrix(rs.getInt("prix_initial"));

		return article;
	}

	/**
	 * Enchere Builder
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private Enchere enchereBuilder(ResultSet rs) throws SQLException {
		Enchere enchere = new Enchere();
		
		enchere.setMontantEnchere(rs.getInt("montant_enchere"));

		return enchere;

	}

	/**
	 * Utilisateur Builder
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private Utilisateur utilisateurBuilder(ResultSet rs) throws SQLException {
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setPseudo(rs.getString("pseudo"));
		return utilisateur;
	}
}
