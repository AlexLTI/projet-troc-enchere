package fr.eni.trocenchere.dal;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.eni.trocenchere.bo.Categorie;

public class CategorieJDBCImpl implements CategorieDAO {

	private static final String SELECT_CATEGORIES = "select no_categorie, libelle FROM CATEGORIES";



	
	public List<Categorie> all() throws SQLException {
		List<Categorie>lstCategories = new ArrayList<Categorie>();
		try (Connection cnx = ConnectionProvider.getConnection()) {
			Statement stmt = cnx.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_CATEGORIES);
			Categorie catCourante = new Categorie();
			while (rs.next()) {
				if(rs.getInt("no_categorie")!= catCourante.getNoCategorie()) {
					catCourante = new Categorie();
					catCourante.setNoCategorie(rs.getInt("no_categorie"));
					catCourante.setLibelle(rs.getString("libelle"));
					lstCategories.add(catCourante);
				}
			}
		}return lstCategories;
	}
}
