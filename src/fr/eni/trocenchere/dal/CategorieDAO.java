/**
 * 
 */
package fr.eni.trocenchere.dal;

import java.sql.SQLException;
import java.util.List;

import fr.eni.trocenchere.bo.Categorie;

/**
 * Classe en charge
 * @author trouat2020
 * @version Encheres - v1.0
 * @date 27 janv. 2021 - 18:30:06
 */
public interface CategorieDAO {
	
	public List<Categorie> all() throws SQLException ;
}
