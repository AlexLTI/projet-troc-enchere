package fr.eni.trocenchere.view;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.trocenchere.bo.Utilisateur;

/**
 * Servlet implementation class Deconnection
 */
@WebServlet("/Deconnection")
public class Deconnection extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session =  request.getSession();
		if(session.getAttribute("userInSession") != null) {
			System.out.println("Before");
			Utilisateur u = (Utilisateur)session.getAttribute("utilisateurEnSession");
			System.out.println(u);
		}
		//Invalider la session
		session.invalidate();
		
		HttpSession sessionAfter = request.getSession();
		System.out.println("After");
		if(sessionAfter.getAttribute("userInSession") != null) {
			Utilisateur u = (Utilisateur)sessionAfter.getAttribute("utilisateurEnSession");
			System.out.println(u);
		}
		
		request.getRequestDispatcher("encheres").forward(request, response);
	}


}
