package fr.eni.trocenchere.view;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.trocenchere.bll.UtilisateurManager;
import fr.eni.trocenchere.bo.Utilisateur;
import fr.eni.trocenchere.exception.BusinessException;

/**
 * Servlet implementation class Connexion
 */
@WebServlet("/Connexion")
public class Connexion extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/seConnecter.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String pseudo = request.getParameter("pseudo");
		String motDePasse = request.getParameter("motDePasse");
		
		
		try {
			Utilisateur u = UtilisateurManager.getUtilisateurManager().validerConnexion(pseudo, motDePasse);
			HttpSession  session =  request.getSession();
			session.setAttribute("utilisateurEnSession",u);
			request.getRequestDispatcher("/encheresconnecte").forward(request, response);
		} catch (BusinessException e) {
			request.setAttribute("errors",e.getErreurs());
			request.getRequestDispatcher("WEB-INF/seConnecter.jsp").forward(request, response);
		}
		
	}

}
