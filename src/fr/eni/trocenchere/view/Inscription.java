package fr.eni.trocenchere.view;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.trocenchere.bll.UtilisateurManager;
import fr.eni.trocenchere.bo.Utilisateur;
import fr.eni.trocenchere.exception.BusinessException;
import fr.eni.trocenchere.exception.DALException;

/**
 * Servlet implementation class Inscription
 */
@WebServlet("/inscription")
public class Inscription extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/inscription.jsp");
		rd.forward(request, response);
	}

	/**
	 * @param u
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> errors = new ArrayList<>();
		/**
		 * Récupère les paramètres rentrés dans la jsp par l'utilisateur
		 */
		String pseudo = request.getParameter("pseudo");
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String email = request.getParameter("email");
		String telephone = request.getParameter("telephone");
		String adresse = request.getParameter("adresse");
		String cp = request.getParameter("cp");
		String ville = request.getParameter("ville");
		String password = request.getParameter("password");
		String confirmerMdp = request.getParameter("confirmMdp");

		/**
		 * Utilisateur Builder 
		 */
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setPseudo(pseudo);
		utilisateur.setNom(nom);
		utilisateur.setPrenom(prenom);
		utilisateur.setEmail(email);
		utilisateur.setTelephone(telephone);
		utilisateur.setRue(adresse);
		utilisateur.setCodePostal(cp);
		utilisateur.setVille(ville);
		utilisateur.setMotDePasse(password);

		/**
		 * Règles de gestion pour vérifier l'intégrité du formulaire d'insription avant de le rentrer dans la DB
		 */
		try {

			UtilisateurManager.getUtilisateurManager().pseudoValide(pseudo, errors);
			UtilisateurManager.getUtilisateurManager().mdpValide(password, confirmerMdp, errors);
			UtilisateurManager.getUtilisateurManager().pseudoOuMailExistant(pseudo, email, errors);

		} catch (DALException | SQLException e) {
			e.printStackTrace();
		}
		/**
		 * Si la liste "errors" ne contient aucunes erreurs à la sortie des règles de gestion, 
		 * on créer l'utilisateur via l'appel de la méthode creerUtilisateur()
		 */
		if (errors.size() == 0) {
			try {
				UtilisateurManager.getUtilisateurManager().creerUtilisateur(utilisateur);
			} catch (DALException | BusinessException e) {
				new BusinessException("erreur impossible de créer l'utilisateur");
				e.printStackTrace();
			}
			RequestDispatcher rd = request.getRequestDispatcher("/encheresconnecte");
			rd.forward(request, response);

		} else {
			/**
			 * Si la liste "errors" contient des lignes on les affiches dans la jsp
			 */
			request.setAttribute("errors", errors);
			request.setAttribute("pseudo", pseudo);
			request.setAttribute("nom", nom);
			request.setAttribute("prenom", prenom);
			request.setAttribute("email", email);
			request.setAttribute("telephone", telephone);
			request.setAttribute("adresse", adresse);
			request.setAttribute("cp", cp);
			request.setAttribute("ville", ville);
			request.getRequestDispatcher("/WEB-INF/inscription.jsp").forward(request, response);
		}

	}

}
