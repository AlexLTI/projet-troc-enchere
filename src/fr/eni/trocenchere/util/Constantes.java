/**
 * 
 */
package fr.eni.trocenchere.util;

/**
 * Classe pour stocker les constantes réutilisables au travers de diverses méthodes
 * 
 * @author mndiaye2020
 * @version TrocEnchere - v1.0
 * @date 25 janv. 2021 - 11:34:04
 */
public interface Constantes {
	String PATTERN_PWD = "^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*[-+!*$@%_])([-+!*$@%_\\w]{8,12})$";

}
