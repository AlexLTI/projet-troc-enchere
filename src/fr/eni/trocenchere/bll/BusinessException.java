package fr.eni.trocenchere.bll;

import java.util.ArrayList;
import java.util.List;

public class BusinessException extends Exception {

	private static final long serialVersionUID = -8104368022479472121L;
	//Ensemble des messages d'erreur de l'affichage
	private List<String> MsgErreur;
	
	
	public void addError(String error) {
		if(MsgErreur == null) {
			MsgErreur = new ArrayList<String>();
		}
		MsgErreur.add(error);
	}
	
	public List<String> getErrors() {
		return MsgErreur;
	}

}
