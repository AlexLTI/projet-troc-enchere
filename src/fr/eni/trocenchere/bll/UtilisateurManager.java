/**
 * 
 */
package fr.eni.trocenchere.bll;

import java.sql.SQLException;
import java.util.List;

import fr.eni.trocenchere.bo.Utilisateur;
import fr.eni.trocenchere.dal.DAOFactory;
import fr.eni.trocenchere.dal.UtilisateurDAO;
import fr.eni.trocenchere.exception.BusinessException;
import fr.eni.trocenchere.exception.DALException;
import fr.eni.trocenchere.util.Constantes;

/**
 * Classe en charge
 * 
 * @author mndiaye2020
 * @version TrocEnchere - v1.0
 * @date 21 janv. 2021 - 21:14:55
 */
public class UtilisateurManager {
	private UtilisateurDAO utilisateurDao;

	private static UtilisateurManager instance;

	/**
	 * Retourne l'instance du manager d'après le pattern singleton
	 * 
	 * @return instance
	 */
	public static UtilisateurManager getUtilisateurManager() {
		if (instance == null) {
			instance = new UtilisateurManager();
		}
		return instance;
	}

	/**
	 * Retourne l'instance de l'interface de la DAO utilisateur
	 */
	private UtilisateurManager() {
		utilisateurDao = DAOFactory.getUtilisateurDAO();
	}

	/**
	 * Méthode en charge de valider la connexion avec un pseudo et mot de passe
	 * 
	 * 
	 * @param pseudo
	 * @param motDePasse
	 * @return u
	 * @throws BusinessException
	 */
	public Utilisateur validerConnexion(String pseudo, String motDePasse) throws BusinessException {

		return utilisateurDao.identification(pseudo, motDePasse);
	}

	/**
	 * Méthode en charge de valider le mot de passe lors de la création d'un
	 * utilisateur
	 * 
	 * @param motDePasse
	 * @param be
	 * @return true ou false(be)
	 */
	public List<String> mdpValide(String motDePasse, String confirmerMdp, List<String> errors) {
		if (motDePasse == null) {
			errors.add("Le mot de passe est obligatoire");
		}

		if (!motDePasse.matches(Constantes.PATTERN_PWD)) {
			errors.add(
					"Le mot de passe doit contenir entre 8 et 12 caractère(1 chiffre, 1 majuscule, 1 caractère spécial) ");
		}

		if (!motDePasse.equals(confirmerMdp)) {
			errors.add("Les mots de passe ne correspondent pas ");
		}

		return errors;
	}

	/**
	 * Méthode en charge de valider le pseudo lors de la création d'un utilisateur
	 * 
	 * @param pseudo
	 * @param be
	 * @return true ou false(be)
	 */
	public List<String> pseudoValide(String pseudo, List<String> errors) {
		if (pseudo == null) {
			errors.add("Le pseudo est obligatoire");
		}
		if (pseudo.isEmpty() || pseudo.length() < 6) {
			errors.add("Le pseudo doit contenir au moins 6 caractères");
		}
		if (pseudo.length() > 30) {
			errors.add("Le pseudo doit contenir au maximum 30 caractères");
		}
		return errors;
	}

	/**
	 * Appel à la méthode en charge de créer un nouvel utilisateur dans la DB
	 * 
	 * @param u
	 * @throws BusinessException
	 * @throws DALException
	 */
	public void creerUtilisateur(Utilisateur u) throws BusinessException, DALException {

		utilisateurDao.creerUtilisateur(u);

	}

	/**
	 * Appel de la méthode en charge de vérifier si le pseudo et le mail ne sont pas
	 * déjà existant lors de la création d'un nouvel utilisateur
	 * 
	 * @param pseudo
	 * @param mail
	 * @param errors
	 * @return List<String> errors
	 * @throws SQLException
	 * @throws DALException
	 */
	public List<String> pseudoOuMailExistant(String pseudo, String mail, List<String> errors)
			throws SQLException, DALException {
		return utilisateurDao.pseudoOuMailExistant(pseudo, mail, errors);
	}

	/**
	 * Appel de la méthode en charge de récupérer le profil d'un utilisateur avec
	 * son pseudo
	 * 
	 * @param pseudo
	 * @return Utilisateur
	 * @throws BusinessException
	 * @throws SQLException
	 */
	public Utilisateur getProfil(String pseudo) throws BusinessException, SQLException {

		return utilisateurDao.getProfil(pseudo);

	}

}
