/**
 * 
 */
package fr.eni.trocenchere.bll;

import java.sql.SQLException;
import java.util.List;

import fr.eni.trocenchere.bo.Categorie;
import fr.eni.trocenchere.dal.CategorieDAO;
import fr.eni.trocenchere.dal.DAOFactory;

/**
 * Classe en charge
 * @author trouat2020
 * @version Encheres - v1.0
 * @date 27 janv. 2021 - 18:31:43
 */
public class CategorieManager {

	private CategorieDAO categorieDAO;
	
	private static CategorieManager instance;
	
	public static CategorieManager getCategorieManager() {
		if(instance == null) {
			instance = new CategorieManager();
		}
		return instance;
		
	}
	
	private CategorieManager() {
		categorieDAO = DAOFactory.getCategorieDAO();
	}
	
	public List<Categorie> all() throws SQLException{
		return categorieDAO.all();
	}
}
