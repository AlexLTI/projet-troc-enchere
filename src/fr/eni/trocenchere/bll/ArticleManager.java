/**
 * 
 */
package fr.eni.trocenchere.bll;

import java.util.List;

import fr.eni.trocenchere.bo.Article;
import fr.eni.trocenchere.dal.ArticleDAO;
import fr.eni.trocenchere.dal.DAOFactory;

/**
 * Classe en charge
 * @author trouat2020
 * @version TrocEncherenew - v1.0
 * @date 21 janv. 2021 - 16:44:15
 */
public class ArticleManager {
	
private ArticleDAO articleVenduDao;

private static ArticleManager instance;

/**
 * Retourne l'instance d'apr�s le pattern singleton
 * @return
 */
public static ArticleManager getArticlesManager() {
	if(instance == null) {
		instance = new ArticleManager();
	}
	return instance;
}

/**
 * 
 */
private ArticleManager () {
	articleVenduDao = DAOFactory.getArticleVenduDAO();
}

/**
 * Appel de la m�thode en charge de r�cup�rer toutes les ench�res en cours 
 * @return List<Article>
 */
public List<Article> findAll(){
	return articleVenduDao.findAll();
}
}
