/**
 * 
 */
package fr.eni.trocenchere.exception;

import java.util.ArrayList;
import java.util.List;


public class BusinessException extends Exception{

	/**
	 * Classe en charge de gérer les exceptions et les erreurs qui s'afficheront
	 * ensuite à l'utilisateur
	 */
	private static final long serialVersionUID = 7635157615696146844L;
	public List<String> MsgErreur;

	public BusinessException() {

	}

	public BusinessException(String string) {
	}

	/**
	 * Méthode en charge de créer une nouvelle liste MsgErreur (Singleton) et
	 * d'jouter le paramètres dans cette liste
	 * 
	 * @param erreur
	 */
	public void addError(String erreur) {
		if (MsgErreur == null) {
			MsgErreur = new ArrayList<String>();
		}
		MsgErreur.add(erreur);
	}

	/**
	 * Méthode en charge de retourner la liste de messages d'erreurs à l'utilisateur
	 * 
	 * @return
	 */
	public List<String> getErreurs() {
		return MsgErreur;
	}
}
