/**
 * 
 */
package fr.eni.trocenchere.bo;

import java.util.List;

/**
 * Classe en charge
 * @author mndiaye2020
 * @version TrocEnchere - v1.0
 * @date 20 janv. 2021 - 14:28:36
 */
public class Categorie {
	private int noCategorie;
	private String libelle;
	private List <Article>article;
	
	/**
	 * Constructeur par défaut.
	 */
	public Categorie() {
		super();
	}

	/**
	 * Constructeur avec paramètres.
	 * @param noCategorie
	 * @param libelle
	 */
	public Categorie(int noCategorie, String libelle) {
		super();
		this.noCategorie = noCategorie;
		this.libelle = libelle;
	}

	/**
	 * Getter pour noCategorie.
	 * @return the noCategorie
	 */
	public int getNoCategorie() {
		return noCategorie;
	}

	/**
	 * Setter pour noCategorie.
	 * @param noCategorie the noCategorie to set
	 */
	public void setNoCategorie(int noCategorie) {
		this.noCategorie = noCategorie;
	}

	/**
	 * Getter pour libelle.
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * Setter pour libelle.
	 * @param libelle the libelle to set
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * Getter pour articleVendu.
	 * @return the articleVendu
	 */
	public List<Article> getArticleVendu() {
		return article;
	}

	/**
	 * Setter pour articleVendu.
	 * @param article the article to set
	 */
	public void setArticleVendu(List<Article> article) {
		this.article = article;
	}

	/**
	* {@inheritDoc}
	*/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((article == null) ? 0 : article.hashCode());
		result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
		result = prime * result + noCategorie;
		return result;
	}

	/**
	* {@inheritDoc}
	*/
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Categorie other = (Categorie) obj;
		if (article == null) {
			if (other.article != null)
				return false;
		} else if (!article.equals(other.article))
			return false;
		if (libelle == null) {
			if (other.libelle != null)
				return false;
		} else if (!libelle.equals(other.libelle))
			return false;
		if (noCategorie != other.noCategorie)
			return false;
		return true;
	}

	/**
	* {@inheritDoc}
	*/
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Categorie [noCategorie=");
		builder.append(noCategorie);
		builder.append(", libelle=");
		builder.append(libelle);
		builder.append(", articleVendu=");
		builder.append(article);
		builder.append("]");
		return builder.toString();
	}
	
	

}
