/**
 * 
 */
package fr.eni.trocenchere.bo;

/**
 * Classe en charge de définir un lieu de retrait pour un article vendu
 * @author mndiaye2020
 * @version TrocEnchere - v1.0
 * @date 20 janv. 2021 - 14:11:43
 */
public class Retrait {
	private int noArticle;
	private String codePostal;
	private String ville;
	private Article article;
	
	/**
	 * Constructeur par défaut.
	 */
	public Retrait() {
		super();
	}

	/**
	 * Constructeur.
	 * @param noArticle
	 * @param codePostal
	 * @param ville
	 * @param article
	 */
	public Retrait(int noArticle, String codePostal, String ville, Article article) {
		super();
		this.noArticle = noArticle;
		this.codePostal = codePostal;
		this.ville = ville;
		this.article = article;
	}

	/**
	 * Getter pour noArticle.
	 * @return the noArticle
	 */
	public int getNoArticle() {
		return noArticle;
	}

	/**
	 * Setter pour noArticle.
	 * @param noArticle the noArticle to set
	 */
	public void setNoArticle(int noArticle) {
		this.noArticle = noArticle;
	}

	/**
	 * Getter pour codePostal.
	 * @return the codePostal
	 */
	public String getCodePostal() {
		return codePostal;
	}

	/**
	 * Setter pour codePostal.
	 * @param codePostal the codePostal to set
	 */
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	/**
	 * Getter pour ville.
	 * @return the ville
	 */
	public String getVille() {
		return ville;
	}

	/**
	 * Setter pour ville.
	 * @param ville the ville to set
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}

	/**
	 * Getter pour articleVendu.
	 * @return the articleVendu
	 */
	public Article getArticleVendu() {
		return article;
	}

	/**
	 * Setter pour articleVendu.
	 * @param article the articleVendu to set
	 */
	public void setArticleVendu(Article article) {
		this.article = article;
	}

	/**
	* {@inheritDoc}
	*/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((article == null) ? 0 : article.hashCode());
		result = prime * result + ((codePostal == null) ? 0 : codePostal.hashCode());
		result = prime * result + noArticle;
		result = prime * result + ((ville == null) ? 0 : ville.hashCode());
		return result;
	}

	/**
	* {@inheritDoc}
	*/
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Retrait other = (Retrait) obj;
		if (article == null) {
			if (other.article != null)
				return false;
		} else if (!article.equals(other.article))
			return false;
		if (codePostal == null) {
			if (other.codePostal != null)
				return false;
		} else if (!codePostal.equals(other.codePostal))
			return false;
		if (noArticle != other.noArticle)
			return false;
		if (ville == null) {
			if (other.ville != null)
				return false;
		} else if (!ville.equals(other.ville))
			return false;
		return true;
	}

	/**
	* {@inheritDoc}
	*/
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Retrait [noArticle=");
		builder.append(noArticle);
		builder.append(", codePostal=");
		builder.append(codePostal);
		builder.append(", ville=");
		builder.append(ville);
		builder.append(", articleVendu=");
		builder.append(article);
		builder.append("]");
		return builder.toString();
	}

}
